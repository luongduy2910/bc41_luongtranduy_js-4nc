/**
 * đầu vào là 3 tọa độ của 3 sinh viên và tọa độ trường học 
 * 
 * xử lý : 
 * DOM và lấy hết các giá trị ô input , ép kiểu sang number bằng cách *1 
 * sử dụng công thức tính quãng đường ,dùng if .... else if .... else để tìm ra quãng đường xa nhất 
 * thông báo tên học sinh đó ra 
 * 
 */

document.getElementById('timhocsinh').addEventListener('click', function(){
    var toadoXsv1 = document.getElementById('sv1-x').value * 1 ; 
    var toadoXsv2 = document.getElementById('sv2-x').value * 1 ; 
    var toadoXsv3 = document.getElementById('sv3-x').value * 1 ; 
    var toadoYsv1 = document.getElementById('sv1-y').value * 1 ; 
    var toadoYsv2 = document.getElementById('sv2-y').value * 1 ; 
    var toadoYsv3 = document.getElementById('sv3-y').value * 1 ; 
    var toadoXTruongHoc = document.getElementById('truonghoc-x').value * 1 ; 
    var toadoYTruongHoc = document.getElementById('truonghoc-y').value * 1 ;
    var tenSv1 = document.getElementById('sv1').value ;
    var tenSv2 = document.getElementById('sv2').value ;   
    var tenSv3 = document.getElementById('sv3').value ;      
    var quangDuongSv1 = Math.sqrt((toadoXTruongHoc - toadoXsv1)*(toadoXTruongHoc - toadoXsv1) + (toadoYTruongHoc - toadoYsv1)*(toadoYTruongHoc - toadoYsv1))
    var quangDuongSv2 = Math.sqrt((toadoXTruongHoc - toadoXsv2)*(toadoXTruongHoc - toadoXsv2) + (toadoYTruongHoc - toadoYsv2)*(toadoYTruongHoc - toadoYsv2))
    var quangDuongSv3 = Math.sqrt((toadoXTruongHoc - toadoXsv3)*(toadoXTruongHoc - toadoXsv3) + (toadoYTruongHoc - toadoYsv3)*(toadoYTruongHoc - toadoYsv3))
    if(quangDuongSv1 > quangDuongSv2 && quangDuongSv1 > quangDuongSv3) {
        document.getElementById('show-ketqua3').innerHTML = `${tenSv1} xa trường nhất`
    }else if (quangDuongSv2 > quangDuongSv1 && quangDuongSv2 > quangDuongSv3) {
        document.getElementById('show-ketqua3').innerHTML = `${tenSv2} xa trường nhất`
    }else {
        document.getElementById('show-ketqua3').innerHTML = `${tenSv3} xa trường nhất`
    }

})