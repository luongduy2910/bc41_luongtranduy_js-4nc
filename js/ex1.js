
/**
 * Đầu vào là ngày tháng năm mà người dùng nhập vào 
 * 
 * xử lý : 
 * DOM tới 3 ô input , lấy giá trị và ép kiểu sang number bằng cách * 1
 * Chia làm 2 trường hợp : tháng có 31 ngày và tháng có 30 ngày 
 * Dùng if lồng if để thu hẹp điều kiện => xử lý bài toán 
 */




// NGÀY MAI 

document.getElementById("ngaymai").onclick = function() {tinhNgayMai()} ;  


function tinhNgayMai() {
    var dateUser = document.getElementById('ngay').value * 1 ; 
    var monthUser = document.getElementById('thang').value * 1 ; 
    var yearUser = document.getElementById('nam').value * 1 ; 

    // Nếu tháng đó có 31 ngày 
    if (monthUser == 1 || monthUser == 3 || monthUser == 5 || monthUser == 7 || monthUser == 8 || monthUser == 10 || monthUser == 12) { 
        if ( dateUser== 31 ) {
            if (monthUser == 12) {
                document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/1/${yearUser + 1}`;
            }
            else {
                document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là  1/${monthUser + 1}/${yearUser}`;
            }
        }
        else {
            document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+1}/${monthUser}/${yearUser} `;
        }
    }
    // Nếu tháng đó có 30 ngày 
    else if(monthUser == 2 || monthUser == 4 || monthUser == 6 || monthUser == 9 || monthUser == 11) {
        if (yearUser % 4 == 0) {
            if(yearUser % 100 == 0) {
                if (yearUser % 400 == 0) {
                    if(monthUser == 2) {
                        if( dateUser== 29) {
                        document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
                    }else {
                        document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
                    }
                    } else {
                        if(dateUser== 30) {
                            document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
                        } else {
                            document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
                        }
                        
                    }
                } else {
                    if(monthUser == 2) {
                        if( dateUser== 28) {
                        document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
                    }else {
                        document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
                    }
                    } else {
                        if(dateUser== 30) {
                            document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
                        } else {
                            document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
                        }
                        
                    }
                }
            } else {
                if(monthUser == 2) {
                    if( dateUser== 29) {
                    document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
                }else {
                    document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
                }
                } else {
                    if(dateUser== 30) {
                        document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
                    } else {
                        document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
                    }
                    
                }
            }
        } else {
            if(monthUser == 2) {
                if( dateUser== 28) {
                document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
            }else {
                document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
            }
            } else {
                if(dateUser== 30) {
                    document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là 1/${monthUser + 1}/${yearUser}`;
                } else {
                    document.getElementById('show-ketqua').innerHTML = `ngày tháng năm tiếp theo là ${dateUser+ 1}/${monthUser}/${yearUser}`;
                }
                
            }
        }
    
    } else {
        document.getElementById('show-ketqua').innerHTML = `Vui lòng điền ngày tháng năm`
    }

}

// NGÀY HÔM QUA 

document.getElementById('homqua').addEventListener('click' , tinhHomqua) ; 

function tinhHomqua() {
    var dateUser = document.getElementById('ngay').value * 1 ; 
    var monthUser = document.getElementById('thang').value * 1 ; 
    var yearUser = document.getElementById('nam').value * 1 ; 
    if(monthUser == 2 || monthUser == 4 || monthUser == 6 || monthUser == 9 || monthUser == 11) {
        if (dateUser == 1) {
            document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là 31/${monthUser -1}/${yearUser}`; 
        }else {
            document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là ${dateUser -1}/${monthUser}/${yearUser}`; 
        }
    }else if (monthUser == 1 || monthUser == 3 || monthUser == 5 || monthUser == 7 || monthUser == 8 || monthUser == 10 || monthUser == 12) {
        if (dateUser ==1 && monthUser == 1) {
            document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là 31/12/${yearUser - 1}`; 
        }
        else if (monthUser == 3 && dateUser == 1) {
            if (yearUser % 4 == 0) {
                if (yearUser % 100 == 0) {
                  if (yearUser % 400 == 0) {
                    document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là 29/${monthUser -1}/${yearUser}`; 
                  } else {
                    document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là 28/${monthUser -1}/${yearUser}`; 
                  }
                } else {
                    document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là 29/${monthUser -1}/${yearUser}`; 
                }
              } else {
                document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là 28/${monthUser -1}/${yearUser}`; 
              }
        }
        else if (dateUser == 1) {
            document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là 30/${monthUser -1}/${yearUser}`; 
        }else {
            document.getElementById('show-ketqua').innerHTML = `ngày hôm qua là ${dateUser -1}/${monthUser}/${yearUser}`; 
        }
    }else {
        document.getElementById('show-ketqua').innerHTML = `Vui lòng nhập ngày tháng năm` ; 
    }    
}











