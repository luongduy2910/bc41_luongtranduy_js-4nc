/**
 * Đầu vào là tháng và năm mà user nhập vào 
 * 
 * Xử lý :
 * DOM tới 2 ô input tháng và năm , lấy giá trị và ép kiểu sang number bằng cách * 1 
 * nếu tháng đó 1 ,3 ,5 ,7 ,8 ,10 ,12 thì có 31 ngày 
 * nếu tháng đó là 1 , 4 , 6 , 9 ,11 thì có 30 ngày 
 * nếu tháng đó là tháng 2 thì sẽ có 2 trường hợp : năm nhuận sẽ là 29 , năm không nhuận sẽ là 28 
 * 
 */

document.getElementById('demsongay').addEventListener('click' , function(){
    var monthUserEl = document.getElementById('month').value * 1 ; 
    var yearUserEl = document.getElementById('year').value * 1 ; 
    if(monthUserEl == 1 || monthUserEl == 3 || monthUserEl == 5 || monthUserEl == 7 || monthUserEl == 8 || monthUserEl == 10 || monthUserEl == 12  ){
        document.getElementById('show-ketqua2').innerHTML = `tháng ${monthUserEl} có 31 ngày` ; 
    }else if (monthUserEl == 4 || monthUserEl == 6 || monthUserEl == 9 || monthUserEl == 11) {
        document.getElementById('show-ketqua2').innerHTML = `tháng ${monthUserEl} có 30 ngày` ; 
    }else {
        if (yearUserEl % 4 == 0) {
            if (yearUserEl % 100 == 0) {
              if (yearUserEl % 400 == 0) {
                document.getElementById('show-ketqua2').innerHTML = `tháng ${monthUserEl} có 29 ngày` ; 
              } else {
                document.getElementById('show-ketqua2').innerHTML = `tháng ${monthUserEl} có 29 ngày` ; 

              }
            } else {
                document.getElementById('show-ketqua2').innerHTML = `tháng ${monthUserEl} có 29 ngày` ; 

            }
          } else {
            document.getElementById('show-ketqua2').innerHTML = `tháng ${monthUserEl} có 28 ngày` ; 

          }
    }
})